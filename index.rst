Hello Sphinx!
===============

This is a practice of "Hello Sphinx" on gitlab pages.

1. create gitlab project

   - https://gitlab.com/shimizukawa/hellosphinx

2. create 'index.rst'

   - https://gitlab.com/shimizukawa/hellosphinx/blob/master/index.rst

3. create 'conf.py'

   - https://gitlab.com/shimizukawa/hellosphinx/blob/master/conf.py

4. create '.gitlab-ci.yml'

   - copied from  https://gitlab.com/pages/sphinx#gitlab-ci
   - And modify bit as: https://gitlab.com/shimizukawa/hellosphinx/blob/master/.gitlab-ci.yml

5. Pipeline will run automatically

   - https://gitlab.com/shimizukawa/hellosphinx/pipelines

6. Done!

   - Generated page: https://shimizukawa.gitlab.io/hellosphinx/
